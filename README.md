# ZenDesk API Scripts
These scripts are used to explore and manage ZenDesk data. 

## How to use
- You'll need Python 3 to run these scripts. 
- Setup your credentials as env vars on your system
- Run with `python3 <dir/file>` such as `python3 requests/an_org.py`   

## Credits
I don't remember where I got the base scripts for this collection but be sure that there is someone smarter behind all 
this. Feel free to add the original authors if you find the source.