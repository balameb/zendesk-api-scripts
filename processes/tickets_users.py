import csv

tickets = []
users = []
ticket_user = []

# Channels
emersec = []
disqus = []
twitsupstat = []
gitlabtweets = []
pivotal = []
stack = []
mailing = []
gitLabforum = []
supportforum = []
reddit = []
quora = []
githost = []

# Tickets
with open('../data/tickets.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        tickets.append(row)

# Users
with open('../data/users.csv') as ucsvfile:
    ureader = csv.DictReader(ucsvfile)
    for urow in ureader:
        users.append(urow)

for ticket in tickets:
    tri = ticket['requester_id']
    for user in users:
        if user['id'] == tri:
            ticket_user.append([ticket['id'], user['email'], ticket['created_at']])

print(ticket_user[0])