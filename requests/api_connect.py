import os

# Set the request parameters
url = os.environ['ZENDESK_API_URL']
user = os.environ['ZENDESK_API_USERNAME'] + '/token'
token = os.environ['ZENDESK_API_TOKEN']
