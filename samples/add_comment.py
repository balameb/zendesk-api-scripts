import json
import requests
import api_connect

# Ticket to update
id = '19757'
body = 'Response through token based API.'

# Package the data in a dictionary matching the expected JSON
data = {
    'ticket': {
        'comment': {
            'body': body
        }
    }
}

# Encode the data to create a JSON payload
payload = json.dumps(data)

# Set url
ticket_url = api_connect.url + 'tickets/' + id + '.json'
headers = {'content-type': 'application/json'}

# Do the HTTP put request
response = requests.put(ticket_url, data=payload, auth=(api_connect.user, api_connect.token), headers=headers)

# Check for HTTP codes other than 200
if response.status_code != 200:
    print('Status:', response.status_code, 'Problem with the request. Exiting.')
    exit()

# Report success
print('Successfully added comment to ticket #{}'.format(id))