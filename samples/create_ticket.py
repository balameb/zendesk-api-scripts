import json
import requests
import api_connect

# New ticket info
subject = 'API test 2'
body = 'Creating tickets through API. - Jose'

# Package the data in a dictionary matching the expected JSON
data = {
    'ticket': {
        'subject': subject,
        'comment': {
            'body': body
        }
    }
}

# Encode the data to create a JSON payload
payload = json.dumps(data)

# Set the request parameters
tickets_url = api_connect.url + 'tickets.json'
headers = {'content-type': 'application/json'}

# Do the HTTP post request
response = requests.post(tickets_url, data=payload, auth=(api_connect.user, api_connect.token), headers=headers)

# Check for HTTP codes other than 201 (Created)
if response.status_code != 201:
    print('Status:', response.status_code, 'Problem with the request. Exiting.')
    exit()

# Report success
print('Successfully created the ticket.')